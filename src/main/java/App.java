import java.util.HashMap;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import static spark.Spark.*;


public class App {
  public static void main(String[] args) {
    staticFileLocation("/public");
    String layout = "templates/layout.vtl";

    get("/", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/index.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    get("/albums", (request, response) -> {
      HashMap model = new HashMap();
      model.put("albums", Album.all());
      model.put("template", "templates/albums.vtl");
      return new ModelAndView(model, "templates/layout.vtl");
    }, new VelocityTemplateEngine());

    post("/albums", (request, response) -> {
        HashMap model = new HashMap();
        String name = request.queryParams("name");
        Album newAlbum = new Album(name);
        model.put("template", "templates/albums.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    get("/albums/new", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/new-album.vtl" );
        //code here
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

  }
}
