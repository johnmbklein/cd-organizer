import java.util.ArrayList;

public class Album {
  private String mName;
  private int mID;
  // private String artist;
  private static ArrayList<Album> AlbumArray = new ArrayList<Album>();

  public Album(String name) {
    mName = name;
    AlbumArray.add(this);
    mID = AlbumArray.size();
  }

  public static ArrayList<Album> all() {
    return AlbumArray;
  }

  public String getName() {
    return mName;
  }

}
