import org.junit.*;
import static org.junit.Assert.*;
import java.io.*;
import java.util.ArrayList;


public class AlbumTest {

  @Test
  public void Album_instantiatesCorrectly_true(){
    Album testAlbum = new Album("Test Album");
    assertEquals(true, testAlbum instanceof Album);
  }

  @Test
  public void Album_instantiatesWithName_true(){
    Album testAlbum = new Album("Test Album");
    assertEquals("Test Album", testAlbum.getName());
  }

  @Test
  public void all_returnsAllInstancesOfAlbum_true() {
    Album firstTestAlbum = new Album("Walk Among Us");
    Album secondTestCd = new Album("American Psycho");
    assertEquals(true, Album.all().contains(firstTestAlbum));
    assertEquals(true, Album.all().contains(secondTestAlbum));
  }

}
